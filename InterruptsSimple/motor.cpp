#include "Arduino.h"
#include "motor.h"
Motor::Motor(){
  _Speed =3;
  _Direction =4;
}
Motor::Motor(int SPEED, int DIRECTION){
  pinMode(SPEED,OUTPUT);
  pinMode(DIRECTION,OUTPUT);

  _Speed =SPEED;
  _Direction =DIRECTION;
}
void Motor::forward(){
  digitalWrite(_Direction,HIGH);
  analogWrite(_Speed,161);
}
void Motor::backward(){
  digitalWrite(_Direction,LOW);
  analogWrite(_Speed,161);
}
void Motor::hault(){
  digitalWrite(_Direction,LOW);
  analogWrite(_Speed,0);
}

