#ifndef motor_h
#define blinker_h

#include "Arduino.h"

class Motor
{
public:
  Motor();
  Motor(int Speed, int Direction);
  void forward();
  void backward();
  void hault ();

private:
  int _Speed;
  int _Direction;
  
};
#endif

